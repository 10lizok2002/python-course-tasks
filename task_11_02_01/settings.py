# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_11_02_01.
#
# Выполнил: Фамилия И.О.
# Группа: !!!
# E-mail: !!!


import json
import datetime
import re
import sys
import os.path


class Settings:
    """Класс Settings хранит настройки приложения.

    Атрибуты класса:
      - DATETIME_FORMAT - формат даты/времени;
      - DATETIME_REGEX - рег. выражение для идентификации даты/времени.

    Атрибуты экземпляра класса:
      - self._data - словарь настроек;
      - self.filename - имя файла настроек;

    Ключи в настройках:
      - "run_count": (int) - кол-во запусков;
      - "last_run_datetime": (datetime.datetime) - дата/время
                                                   последнего запуска;
      - "last_run_platform_info": (tuple) - информация о платформе
                                            последнего запуска.

    Пример файла настроек:
    {
        "last_run_datetime": "2016-01-25 18:10:34",
        "run_count": 5,
        "last_run_platform_info": [
            "Windows",
            "user-pc",
            "10",
            "10.0.14393",
            "AMD64",
            "Intel64 Family 6 Model 58 Stepping 9, GenuineIntel"
        ]
    }
    """

    raise NotImplementedError
    # Уберите raise и дополните константы ниже
    # Формат даты: 2016-01-25 18:10:34
    DATETIME_FORMAT = !!!
    # Регулярное выражения для поиска даты формата DATETIME_FORMAT
    DATETIME_REGEX = !!!

    def __init__(self):
        """Инициализация класса.

        Действия:
          - инициализировать словарь 'self._data';
          - инициализировать имя файла настроек в "settings.json"
            в папке скрипта (должно быть получено абсолютное имя).
        """
        raise NotImplementedError
        # Уберите raise и добавьте необходимый код

    def __str__(self):
        """Вернуть настройки 'self._data' в виде JSON-строки."""
        raise NotImplementedError
        # Уберите raise и добавьте необходимый код

    def set_value(self, name, value):
        """Установить для параметра 'name' (str) значение 'value'.

        Если 'value' - дата/время, необходимо предварительно выполнить
        преобразование в строку по формату 'self.DATETIME_FORMAT'.
        """
        raise NotImplementedError
        # Уберите raise и добавьте необходимый код

    def get_value(self, name, default=None):
        """Вернуть значение параметра 'name' (str).

        Если 'name' найдено, тип значения - str и
            указывает на дату/время (self.DATETIME_REGEX) -
            выполнить преобразование из str в datetime.datetime
            по формату 'self.DATETIME_FORMAT';
        Если 'name' не найдено, возвращается 'default'.
        """
        raise NotImplementedError
        # Уберите raise и добавьте необходимый код

    def load(self):
        """Загрузить настройки из файла 'self.filename'."""
        raise NotImplementedError
        # Уберите raise и добавьте необходимый код

    def save(self):
        """Сохранить настройки в файл 'self.filename'."""
        raise NotImplementedError
        # Уберите raise и добавьте необходимый код
